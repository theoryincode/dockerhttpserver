FROM scratch
COPY HTTPServer /
EXPOSE 8080/tcp
CMD ["/HTTPServer"]
